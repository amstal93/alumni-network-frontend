export interface IUserInfo {
	name: string;
	picture: string;
	workStatus: string;
	bio: string;
	funFact: string;
}

export interface IUpdateUser {
	id: number;
	body: IUserInfo;
}
