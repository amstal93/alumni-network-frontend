import { IPostBody } from "./../../api/post/postApiTypes";
import {
	createPostRequest,
	fetchPosts,
	fetchPostsForUserId,
} from "./../../api/post/postApi";
import { DataStatus } from "./../../common/commonTypes";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";
import { IPost, IPostsById } from "./postTypes";

/**
 * Fetch all posts thunk
 */
export const fetchAllPosts = createAsyncThunk(
	"post/fetchPosts",
	async (_, thunkAPI) => {
		const response = await fetchPosts();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error fetching posts",
			});
		}
		return response.data;
	}
);

/**
 * Fetch all posts for the currently logged in user
 */
export const fetchAllPostsForLoggedInUser = createAsyncThunk(
	"post/fetchPostsForLoggedInUser",
	async (userId: number, thunkAPI) => {
		const response = await fetchPostsForUserId(userId);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error fetching posts",
			});
		}
		return response.data;
	}
);

/**
 * Create Post thunk
 */
export const createPost = createAsyncThunk(
	"post/createPost",
	async (post: IPostBody, thunkAPI) => {
		const response = await createPostRequest(post);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error creating post",
			});
		}
		return response.data;
	}
);

/**
 * Interface for state of this slice
 */
interface PostState {
	postsStatus: DataStatus;
	postIds: string[];
	postById: {
		[id: number]: IPost;
	};
	activePostId?: string;
}

/**
 * Sets the initialState
 */
const initialState: PostState = {
	postsStatus: DataStatus.Idle,
	postIds: [],
	postById: {},
};

/**
 * The postSLice. This is a small part of the redux store.
 */
export const postSlice = createSlice({
	name: "posts",
	initialState,
	reducers: {
		setActivePostId: (state, action: PayloadAction<string | undefined>) => {
			if (action.payload !== undefined)
				if (state.activePostId === action.payload)
					state.activePostId = undefined;
				else state.activePostId = action.payload;
			else state.activePostId = undefined;
		},
		setPostsStatus: (state, action: PayloadAction<DataStatus>) => {
			state.postsStatus = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchAllPosts.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: IPostsById, post: IPost) => {
				byId[post.postId] = post;
				return byId;
			}, {});
			state.postById = byId;
			state.postIds = Object.keys(byId);
			state.postsStatus = DataStatus.Succeeded;
		});
		builder.addCase(fetchAllPostsForLoggedInUser.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: IPostsById, post: IPost) => {
				byId[post.postId] = post;
				return byId;
			}, {});
			state.postById = byId;
			state.postIds = Object.keys(byId);
			state.postsStatus = DataStatus.Succeeded;
		});
		builder.addCase(createPost.fulfilled, (state, action) => {
			state.postIds.push(action.payload.postId);
			state.postById = Object.assign(state.postById, {
				[action.payload.postId]: action.payload,
			});
		});
	},
});

export const { setActivePostId, setPostsStatus } = postSlice.actions;

export const selectActivePostId = (state: RootState) =>
	state.posts.activePostId;
export const selectPostsIds = (state: RootState) => state.posts.postIds;
export const selectPostsStatus = (state: RootState) => state.posts.postsStatus;
export const selectPostById = (state: RootState) => state.posts.postById;

export const selectFilteredAndSortedByLastUpdatedPostIds = (
	state: RootState
) => {
	let filteredPostIds: string[] = [];
	const query = state.search.searchQuery;
	state.posts.postIds.forEach((id) => {
		const post = state.posts.postById[parseInt(id)];
		const creator = state.allUsers.userById[post.senderId];
		const targetedGroup =
			post.target_group && state.group.groupById[post.target_group];

		if (
			post.postMessage.toLowerCase().includes(query.toLowerCase()) ||
			post.title.toLowerCase().includes(query.toLowerCase()) ||
			creator.name.toLowerCase().includes(query.toLowerCase()) ||
			(targetedGroup &&
				targetedGroup.name.toLowerCase().includes(query.toLowerCase()))
		)
			filteredPostIds.push(id);
	});
	sortPostsByLastUpdated(state, filteredPostIds);
	return filteredPostIds;
};

const sortPostsByLastUpdated = (state: RootState, postsIds: string[]) => {
	postsIds.sort((a: string, b: string) => {
		const byId = state.posts.postById;
		const first = byId[parseInt(a)];
		const second = byId[parseInt(b)];
		return (
			new Date(second.lastUpdated).valueOf() -
			new Date(first.lastUpdated).valueOf()
		);
	});
};

export default postSlice.reducer;
