export interface IEvent {
	eventId: number;
	title: string;
	createdBy: number;
	lastUpdated: string;
	description: string;
	allowGuests: boolean;
	startTime: string;
	endTime: string;
	bannerImg?: string;
	users?: number[];
	groups?: number[];
	topics?: number[];
	rsvps?: number[];
	posts?: number[];
}

export interface IEventById {
	[id: number]: IEvent;
}
