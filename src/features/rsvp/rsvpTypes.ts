export interface IRsvp {
	lastUpdated: string;
	guestCount: number;
	rsvpid: number;
	alumni: number;
	event?: number;
}

export interface IRsvpById {
	[id: number]: IRsvp;
}
