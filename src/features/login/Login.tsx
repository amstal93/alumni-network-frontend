import React, { useEffect, useState } from "react";
import { selectLoginMessage, setLoginMessage } from "./loginSlice";
import { Grid, Button, Typography, Paper } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { useNavigate } from "react-router-dom";
import {
	fetchUserInfo,
	fetchUserInfoWithToken,
	selectUserId,
} from "../user/userSlice";
import { useAuth0 } from "@auth0/auth0-react";

/**
 * The login component with local states and login functionality
 * @returns the login component
 */
const Login = () => {
	const dispatch = useAppDispatch();
	const navigate = useNavigate();
	const { isAuthenticated } = useAuth0();

	const { loginWithRedirect, user } = useAuth0();

	const loginMessage = useAppSelector(selectLoginMessage);

	const loggedInUserId = useAppSelector(selectUserId);
	const [requestMessage, setRequestMessage] = useState("");

	/**
	 * Login function
	 * First: login with auth0
	 * Second: tries to fetch user info based on the generated token
	 */
	const userLogin = () => {
		console.log("logging in...");
		setLoginMessage("Logging in...");
		loginWithRedirect().finally(() => {
			console.log("Finally");
			dispatch(fetchUserInfoWithToken);
		});
	};

	// navigates the user after being authenticated and its user data is fetched
	useEffect(() => {
		if (isAuthenticated && loggedInUserId) navigate("/timeline");
	}, [isAuthenticated, loggedInUserId, navigate]);

	/*	const handleTestLoginClick = () => {
		setRequestMessage("Logging in...");
		dispatch(fetchUserInfo(id))
			.unwrap()
			.then(() => {
				setRequestMessage("Successfully logged in");
			})
			.catch((error) => {
				console.log(error);
				setRequestMessage(
					error.errorMessage + ". Make sure you have an accessToken first"
				);
			});
	};*/

	return (
		<Grid
			container
			justifyContent="center"
			justifyItems="center"
			alignItems="center"
			alignContent="center"
			direction="column"
			sx={{ height: "90vh", width: "100vw" }}
			spacing={2}>
			<Grid item>
				<img
					style={{ width: 500, height: 500 }}
					src="https://icon-library.com/images/network-icon-png/network-icon-png-20.jpg"
					alt=""
				/>
			</Grid>
			<Grid item>
				<Grid
					container
					direction="column"
					justifyItems="center"
					alignItems="center">
					<Grid item>
						<Typography variant="h6">
							Welcome to the Alumni Network Portal
						</Typography>
					</Grid>
					<Grid item>
						<Typography>
							Alumni Network is a social platform for previous candidates of the
							Experis Academy Fullstack Developer Course at Noroff
						</Typography>
					</Grid>
				</Grid>
			</Grid>
			<Grid item>
				<Button variant="contained" onClick={userLogin}>
					Login
				</Button>
			</Grid>
			{(requestMessage || loginMessage) && (
				<Paper sx={{ padding: 2, mt: 2 }}>
					{requestMessage && (
						<Typography
							color={requestMessage.toLowerCase().includes("err") ? "red" : ""}>
							{requestMessage}
						</Typography>
					)}
					{loginMessage && (
						<Typography
							color={loginMessage.toLowerCase().includes("err") ? "red" : ""}>
							{loginMessage}
						</Typography>
					)}
				</Paper>
			)}
		</Grid>
	);
};

export default Login;
