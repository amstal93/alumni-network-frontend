import React, { useState } from "react";
import Calendar from "react-calendar";
import { Box, Container, Paper } from "@mui/material";
import "./Calendar.css";

const UserCalendar = () => {
	const [dateState, setDateState] = useState<Date>(new Date());

	return (
		<Container>
			<Box>
				<Box
					sx={{
						display: "flex",
						flexDirection: "column",
						alignItems: "center",
						marginTop: 10,
					}}>
					<Paper elevation={3}>
						<Calendar onChange={setDateState} value={dateState} />
					</Paper>
				</Box>
			</Box>
		</Container>
	);
};

export default UserCalendar;
