import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export interface CalendarState {
	date: string;
}

const initialState: CalendarState = {
	date: "",
};

export const calendarSlice = createSlice({
	name: "date",
	initialState,
	reducers: {
		setDate: (state, action: PayloadAction<string>) => {
			state.date = action.payload;
		}
	},
	extraReducers: (builder) => {},
});

export const { setDate } = calendarSlice.actions;

// Selectors
export const selectDate = (state: RootState) => state.calendar.date;

export default calendarSlice.reducer;