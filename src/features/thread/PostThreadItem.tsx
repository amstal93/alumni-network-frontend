import { Avatar, Divider, Grid, Tooltip, Typography } from "@mui/material";
import React from "react";
import { useAppSelector } from "../../app/hooks";
import { convertDate } from "../../common/helperFunctions";

interface ThreadItemProps {
	id: number;
	threadId: string;
	handleUsernameClick: (userId: string) => void;
}

/**
 * Individual post thread item (a reply)
 * @param props
 * @returns a post thread item (a reply)
 */
const PostThreadItem = (props: ThreadItemProps) => {
	const post = useAppSelector((state) => state.posts.postById[props.id]);
	const user = useAppSelector(
		(state) => state.allUsers.userById[post.senderId]
	);

	const usernameClick = () => {
		props.handleUsernameClick(user.userId.toString());
	};

	return (
		<>
			<Divider variant="inset" sx={{ mb: 1, mt: 1 }} />
			<Grid container alignItems="center" sx={{ mb: 1 }}>
				<Grid item sx={{ mr: 2 }}>
					<Grid container>
						<Avatar src={user.picture} alt=""></Avatar>
					</Grid>
				</Grid>
				<Grid item xs={10}>
					<Grid container direction="column">
						<Grid item>
							<Grid container alignItems="center">
								<Tooltip
									placement="top"
									title={"Go to " + user.name + "'s profile"}>
									<Typography
										onClick={usernameClick}
										variant="h6"
										sx={{
											mr: 2,
											padding: 0.5,
											borderRadius: 4,
											"&:hover": { cursor: "pointer", bgcolor: "lavender" },
										}}>
										{user.name}
									</Typography>
								</Tooltip>
								<Tooltip title={post.lastUpdated}>
									<Typography>{convertDate(post.lastUpdated)}</Typography>
								</Tooltip>
							</Grid>
						</Grid>
						<Grid item>
							<Typography flexWrap={"wrap"}>- {post.postMessage}</Typography>
						</Grid>
					</Grid>
				</Grid>
			</Grid>
		</>
	);
};

export default PostThreadItem;
