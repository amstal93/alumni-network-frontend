import { RootState } from "./../../app/store";
import { IUpdateUser } from "./../../api/user/userTypes";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getUserById, loginUser, updateUser } from "../../api/user/userApi";
import { IUser } from "../allUsers/usersTypes";

const initialState: IUser = {
	userId: 0,
	name: "",
	picture: "",
	status: "",
	bio: "",
	funFact: "",
};

export const fetchUserInfo = createAsyncThunk(
	"user/getUserInfo",
	async (id: number, thunkAPI) => {
		const response = await getUserById(id);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error fetching user info",
			});
		}
		return response.data;
	}
);

export const fetchUserInfoWithToken = createAsyncThunk(
	"user/getUserInfoWithToken",
	async (_, thunkAPI) => {
		const response = await loginUser();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error logging in",
			});
		}
		return response.data;
	}
);

export const updateUserInfo = createAsyncThunk(
	"user/update",
	async (req: IUpdateUser, thunkAPI) => {
		const response = await updateUser(req);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error updating user",
			});
		}
		return response.data;
	}
);

export const userSlice = createSlice({
	name: "user",
	initialState,
	reducers: {
		setName: (state, action: PayloadAction<string>) => {
			state.name = action.payload;
		},
		setWorkStatus: (state, action: PayloadAction<string>) => {
			state.status = action.payload;
		},
		setBio: (state, action: PayloadAction<string>) => {
			state.bio = action.payload;
		},
		setFunFact: (state, action: PayloadAction<string>) => {
			state.funFact = action.payload;
		},
		logoutAction: (state) => {
			state.userId = 0;
			state.name = "";
			state.bio = "";
			state.funFact = "";
			state.picture = "";
			state.status = "";
			state.events = undefined;
			state.topics = undefined;
			state.posts = undefined;
			state.groups = undefined;
			state.rsvps = undefined;
		},
		subscribeOrUnsubscribeToTopic: (state, action: PayloadAction<number>) => {
			if (state.topics?.includes(action.payload)) {
				const index = state.topics?.indexOf(action.payload);
				if (index !== undefined) state.topics?.splice(index, 1);
			} else {
				state.topics?.push(action.payload);
			}
		},
		leaveGroup: (state, action: PayloadAction<number>) => {
			if (state.groups?.includes(action.payload)) {
				const index = state.groups?.indexOf(action.payload);
				if (index !== undefined) state.groups.splice(index, 1);
			}
		},
		joinGroup: (state, action: PayloadAction<number>) => {
			state.groups?.push(action.payload);
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchUserInfoWithToken.fulfilled, (state, action) => {
			state.userId = action.payload.userId;
			state.name = action.payload.name;
			state.bio = action.payload.bio;
			state.funFact = action.payload.funFact;
			state.status = action.payload.status;
			state.picture = action.payload.picture;
			state.events = action.payload.events;
			state.groups = action.payload.groups;
			state.posts = action.payload.posts;
			state.topics = action.payload.topics;
			state.rsvps = action.payload.rsvps;
		});
	},
});

export const {
	setName,
	setWorkStatus,
	setBio,
	setFunFact,
	subscribeOrUnsubscribeToTopic,
	logoutAction,
	joinGroup,
	leaveGroup,
} = userSlice.actions;

// Selectors
export const selectName = (state: RootState) => state.user.name;
export const selectWorkStatus = (state: RootState) => state.user.status;
export const selectBio = (state: RootState) => state.user.bio;
export const selectFunFact = (state: RootState) => state.user.funFact;
export const selectUserId = (state: RootState) => state.user.userId;
export const selectUserTopics = (state: RootState) => state.user.topics;
export const selectUserGroups = (state: RootState) => state.user.groups;

export const selectProfilePicture = (state: RootState) => state.user.picture;

export default userSlice.reducer;
