import React from "react";
import UserSettings from "../features/user/UserSettings";
import withAuth from "../hoc/withAuth";

const UserSettingsView = () => {
	return (
		<div>
			<UserSettings></UserSettings>
		</div>
	);
};

export default withAuth(UserSettingsView);
