import { Navigate } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";

/**
 * Checks if you are authenticated before giving access
 * @param {*} Component
 * @returns
 */
const withAuth = (Component) => (props) => {
	const { isAuthenticated } = useAuth0();

	if (isAuthenticated) {
		return <Component {...props} />;
	} else {
		return <Navigate to="/" />;
	}
};

export default withAuth;
